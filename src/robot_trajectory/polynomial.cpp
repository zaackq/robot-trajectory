#include <umrob/polynomial.h>
#include <iostream>
#include <eigen3/Eigen/Dense>
using std::pow;
/*Zakaria Bouzit @Zackq

/**************************************************************/
namespace umrob {
    Polynomial::Constraints& Polynomial::constraints() {
    return constraints_;}

const Polynomial::Constraints& Polynomial::constraints() const {
    return constraints_;
}

/********************** TO DO1 **********************************/
void Polynomial::computeCoefficients() {
    // here [done]
    
    //1 simplified:
    double dx = constraints_.xf - constraints_.xi; 
    if ( constraints_.yi == 0 ){
        //a = -(12*yi - 12*yf + 6*dx*dyf + 6*dx*dyi - d2yf*dx^2 + d2yi*dx^2)/(2*dx^5)
        coefficients_.a = -12 * (constraints_.yi) - 12.*(constraints_.yf ) + 6.*(dx)*(constraints_.dyf) + 6.*(dx)*(constraints_.dyi) - (constraints_.d2yf)*pow(dx,2) + (constraints_.d2yi)*pow(dx,2)/(2.*pow(dx,5)) ;
        //b = (30*yi - 30*yf + 14*dx*dyf + 16*dx*dyi - 2*d2yf*dx^2 + 3*d2yi*dx^2)/(2*dx^4) 
        coefficients_.b = 30 * constraints_.yi - 30.*constraints_.yf + 14.*dx*constraints_.dyf + 16.*dx*constraints_.dyi - 2.*constraints_.d2yf*pow(dx,2) + 3.*constraints_.d2yi*(pow(dx,2))/(2.*pow(dx,4)) ; 
        //c = -(20*yi - 20*yf + 8*dx*dyf + 12*dx*dyi - d2yf*dx^2 + 3*d2yi*dx^2)/(2*dx^3) 
        coefficients_.c = -20 * constraints_.yi - 20*constraints_.yf + 8*dx*constraints_.dyf + 12*dx*constraints_.dyi - constraints_.d2yf*pow(dx,2) + 3*constraints_.d2yi*pow(dx,2)/(2.*pow(dx,3)) ;
        //d = d2yi/2 
        coefficients_.d = 0.5 * constraints_.d2yi;        
        //e = dyi 
        coefficients_.e = constraints_.dyi  ;
        //f = yi 
        coefficients_.f = constraints_.yi  ;
    }
    //2 general:
    else{
        Eigen::Matrix<double,6,6> A;
        Eigen::Matrix<double,6,1> P;

        A(0,0) = 0; A(0,1) = 0; A(0,2) = 0; A(0,3) = 0; A(0,4) = 0; A(0,5) = 1;
        A(1,0) = 0; A(1,1) = 0; A(1,2) = 0; A(1,3) = 0; A(1,4) = 1; A(1,5) = 0;
        A(2,0) = 0; A(2,1) = 0; A(2,2) = 0; A(2,3) = 2; A(2,4) = 0; A(2,5) = 0;
        A(3,0) = pow(dx,5); A(3,1) = pow(dx,4); A(3,2) = pow(dx,3); A(3,3) = pow(dx,2); A(3,4) = dx; A(3,5) = 1; 
        A(4,0) = 5*pow(dx,4); A(4,1) = 4*pow(dx,3); A(4,2) = 3*pow(dx,2); A(4,3) = 2*dx; A(4,4) = 1; A(4,5) = 0;
        A(5,0) = 20*pow(dx,3); A(5,1) = 12*pow(dx,2); A(5,2) = 6*dx; A(5,3) = 2; A(5,4) = 0; A(5,5) = 0;

        Eigen::Matrix<double,6,6> Ai = A.inverse();   

        P(0,0) = constraints_.yi;
        P(1,0) = constraints_.dyi;
        P(2,0) = constraints_.d2yi;
        P(3,0) = constraints_.yf;
        P(4,0) = constraints_.dyf;
        P(5,0) = constraints_.d2yf;
        
        Eigen::Matrix<double,6,1> C = Ai*P;

        coefficients_.a = C(0,0);
        coefficients_.b = C(1,0);
        coefficients_.c = C(2,0);
        coefficients_.d = C(3,0);
        coefficients_.e = C(4,0);
        coefficients_.f = C(5,0);
    }

}

/********************** TO DO2 **********************************/
double Polynomial::evaluate(double x) {
    // here []
    bool still = false;
    if (constraints_.xi < constraints_.xf) {still = true;}

    if (still) {
        if(x < constraints_.xi) {return constraints_.yi;}
        else {return constraints_.yf;}
    }
    else if (!still) {
        if (x > constraints_.xi) {return constraints_.yi;}
        else {return constraints_.yf;}
    }
    double dx = x - constraints_.xi;
    //! y = ax^5 + bx^4 + cx^3 + dx^2 + ex + f
    /**************** nassim put's x instead of dx*/
    return coefficients_.a * pow(dx, 5) + coefficients_.b * pow(dx, 4) + coefficients_.c * pow(dx, 3) + coefficients_.d * pow(dx, 2) + coefficients_.e *dx + coefficients_.f;
}

/********************** TO DO3 **********************************/
double Polynomial::evaluateFirstDerivative(double x) {
    // here[]
    bool still = false;
    if (constraints_.xi < constraints_.xf) {still = true;}
    
    if (still) {
        if (x < constraints_.xi) { return constraints_.dyi; }
        else { return constraints_.dyf; }
        }
    else if (!still){
        if (x > constraints_.xi) { return constraints_.dyi; }
        else { return constraints_.dyf; }
    }
    double dx = x - constraints_.xi;
    //! dy = 5ax^4 + 4bx^3 + 3cx^2 + 2dx + e
    return 5 * coefficients_.a * pow(dx, 4) + 4 * coefficients_.b * pow(dx, 3) + 3 * coefficients_.c * pow(dx, 2) + 2 * coefficients_.d * dx + coefficients_.e;
}

/********************** TO DO4 **********************************/
double Polynomial::evaluateSecondDerivative(double x) {
    // here[]
    bool still = false;
    if (constraints_.xi < constraints_.xf) {still = true;}
    if (still) {
        if (x < constraints_.xi) { return constraints_.d2yi; }
        else { return constraints_.d2yf; }
    }
    else if (!still){
        if (x > constraints_.xi) { return constraints_.d2yi; }
        else { return constraints_.d2yf; }
    }
    double dx = x - constraints_.xi;
    //! d2y = 20ax^3 + 12bx^2 + 6cx + 2d
    return 20 * coefficients_.a * pow(dx, 3) + 12 * coefficients_.b * pow(dx, 2) + 6 * coefficients_.c * dx + 2 * coefficients_.d;
}
}
